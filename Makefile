up:
	docker-compose up --build

down:
	docker-compose down

test:
	docker-compose run --rm lovelace rspec

lock:
	docker-compose run --rm lovelace bundle lock

command:
	docker-compose run --rm lovelace $(args)

build:
	docker-compose build
