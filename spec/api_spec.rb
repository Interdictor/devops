require 'faraday'

RSpec.describe 'API' do
  it 'is up and running' do
    url = 'http://lovelace:4567'

    response = Faraday.get(url)

    expect(response.body).to eq('hello world')
  end
end
