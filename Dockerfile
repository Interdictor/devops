FROM ruby:2.6.4-slim-buster

ENV WORKPATH /opt/app/
ENV RUBYLIB $WORKPATH
ENV USERNAME servo

RUN adduser --uid 1000 --system --disabled-password --group $USERNAME

WORKDIR $WORKPATH
RUN chown -R $USERNAME $WORKPATH
USER $USERNAME

COPY --chown=servo:servo Gemfile* $WORKPATH
RUN bundle install
COPY --chown=servo:servo . $WORKPATH

CMD ["ruby", "src/main.rb"]
